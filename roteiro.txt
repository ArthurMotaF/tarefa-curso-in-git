git help: Mostra vários comandos de forma resumida

git help <comando>: Mostra detalhes de um comando específico

git config --global user.name “Seu Nome”
git config --global user.email “seuemaill@gmail.com”   (Usar o mesmo email do gitlab)
 
Para o repositório atual: --local;
Para o Usuário: --global;
Para o Computador: --system

Opcional:
(primeiro testa code -v)
git config --global core.editor 'code --wait' -> Vscode para editor padrão (precisa do code no PATH)


	Se tudo der certo:
	git config --global -e -> Abre o arquivo de configuração no vscode

-------------------------------------------------------


git init: Cria um repositório na pasta atual monitorada pelo Git.

git status: Mostra detalhes sobre o seu repositório, mostrando modificações em arquivos e branchs.

git add <arquivo/diretório>: Adiciona o arquivo/diretório para a área de preparação.

git add --all = git add . : Adiciona todos arquivo/diretório modificados e novos para a área de preparação.

git commit -m “Primeiro commit”: Cria um novo commit com as alterações na área de preparação e especifica a mensagem do commit em linha.

-------------------------------------------------------

git log: Lista todos os commits do branch atual.

git log <arquivo>: Lista o histórico de commits de um arquivo selecionado.

git reflog: Lista todos os commits de forma resumida.

-------------------------------------------------------

git show: Mostra as alterações feitas no último commit.

git show <commit>: Mostra as alterações feita de um commit selecionado.

-------------------------------------------------------

git diff: Mostra as alterações anteriores ao commit.

git diff <commit1> <commit2>: Mostra as diferenças entre os dois commits selecionados.

-------------------------------------------------------

git reset --hard <commit>: Retorna com a vesão de um commit selecionado.

-------------------------------------------------------

git branch: Lista os branchs locais.

git branch -r: lista os branchs remotos.

git branch -a: Lista todos os branchs, sendo locais ou remotos.

git branch -d <branch_name>: Apaga um branch caso tenha salvo em commit.

git branch -D <branch_name>: Apaga um branch independente do status de modificação.

git branch -m <nome_novo>: Renomeia a branch atual.

git branch -m <nome_antigo> <nome_novo>: Renomeia a branch selecionada.

-------------------------------------------------------

git checkout <branch_name>: Abre a branch selecionada.

git checkout -b <branch_name>: Cria uma branch e a abre.

-------------------------------------------------------

git merge <branch_name>: Mescla duas branchs.

-------------------------------------------------------

git clone: Clona um repositório remoto em uma pasta local.

git pull: Baixa os commits do repositório remoto ara o local.

git push: Envia os commits locais para o repositório remoto.

-------------------------------------------------------

git remote -v: Mostra as variáveis e URLs atreladas a elas.

git remote add origin <url>: Atrela uma URL a uma variável.

git remote <url> origin: Altera a URL da variável.

--------------------------------------------------------

Documentação do git:
https://git-scm.com/doc

Playlist GIT:
https://www.youtube.com/playlist?list=PLucm8g_ezqNq0dOgug6paAkH0AQSJPlIe

Vídeo sobre Git: 
https://www.youtube.com/watch?v=kB5e-gTAl_s
